<?php
/*
 *  *Plugin Name: Plan
 *  Author: Marcin Dominiak
 */

add_shortcode('Plan', 'plan');

function plan()
{
    if (!session_id())
        session_start();
    $plan = $_SESSION['plan'];
    $myId = $_COOKIE['currID'];

    $conn = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

    if ($conn->connect_error)
    {
        die("Connection failed: ". $conn->connect_error);
    }


    $res = $conn->query("SELECT * FROM Extra_Plan WHERE ID = '$myId' AND
        day >= CURDATE() ORDER BY day ASC, beggining ASC");
    $extraBeg = array();
    $extraEnd = array();
    $extraDesc = array();
    $extraDay = array();
    $it = 0;
    while($row = $res->fetch_array())
    {
        $extraBeg[$it] = $row['beggining'];
        $extraEnd[$it] = $row['finnish'];
        $extraDay[$it] = $row['day'];
        $extraDesc[$it] = $row['activity'];
        $it++;
    }
    $maxIt = $it;
    $it = 0;

    $out = "<table>";

    $it2 = 0;
    while ($it2 < count($plan) && $it < $maxIt)
    {
        $currDayP = DateTime::createFromFormat('Y-m-d H:i:s', $plan[$it2]->start_time);
        $currDayP = date_format($currDayP, 'Y-m-d');
        $currDayE = $extraDay[$it];

        $startP = date_format(DateTime::createFromFormat('Y-m-d H:i:s', $plan[$it2]->start_time), 'H:i');
        $startE = $extraBeg[$it];

        if ($currDayP < $currDayE || ($currDayP == $currDayE && $startP < $startE))
        {
            $currDay = $currDayP;
            if (isset($prevDay))
                if ($currDay != $prevDay)
                    $out .= "<tr><th>" . date('D',  strtotime($currDay)) . "</th></tr>";
            if (!isset($prevDay))
                $out .= "<tr><th>" . date('D',  strtotime($currDay)) . "</th></tr>";

            $prevDay = $currDay;

            $end = DateTime::createFromFormat('Y-m-d H:i:s', $plan[$it2]->end_time);
            $end = date_format($end, 'H:i');
            $out .= "<tr><td>" . $startP . "</td><td>" . $end . "</td><td>";
            $out .= $plan[$it2]->name->pl . "</td></tr>";
            $it2++;
        }
        else
        {
            $currDay = $currDayE;
            if (isset($prevDay))
                if ($currDay != $prevDay)
                    $out .= "<tr><th>" . date('D',  strtotime($currDay)) . "</th></tr>";
            if (!isset($prevDay))
                $out .= "<tr><th>" . date('D',  strtotime($currDay)) . "</th></tr>";

            $prevDay = $currDay;

            $extraBeg[$it] = DateTime::createFromFormat('H:i:s', $extraBeg[$it]);
            $extraBeg[$it] = date_format($extraBeg[$it], 'H:i');
            $extraEnd[$it] = DateTime::createFromFormat('H:i:s', $extraEnd[$it]);
            $extraEnd[$it] = date_format($extraEnd[$it], 'H:i');
            $out .= "<tr><td>" . $extraBeg[$it] . "</td><td>" . $extraEnd[$it] . "</td><td>" . $extraDesc[$it] . "</td></tr>";
            $it++;
        }
    }

    while ($it2 < count($plan))
    {
        $currDayP = DateTime::createFromFormat('Y-m-d H:i:s', $plan[$it2]->start_time);
        $currDayP = date_format($currDayP, 'Y-m-d');
        $currDay = $currDayP;
        if (isset($prevDay))
            if ($currDay != $prevDay)
                $out .= "<tr><th>" . date('D',  strtotime($currDay)) . "</th></tr>";
        if (!isset($prevDay))
            $out .= "<tr><th>" . date('D',  strtotime($currDay)) . "</th></tr>";

        $prevDay = $currDay;

        $end = DateTime::createFromFormat('Y-m-d H:i:s', $plan[$it2]->end_time);
        $end = date_format($end, 'H:i');
        $out .= "<tr><td>" . $startP . "</td><td>" . $end . "</td><td>";
        $out .= $plan[$it2]->name->pl . "</td></tr>";
        $it2++;
    }

    while ($it < $maxIt)
    {
        $currDayE = $extraDay[$it];

        $currDay = $currDayE;
        if (isset($prevDay))
            if ($currDay != $prevDay)
                $out .= "<tr><th>" . date('D',  strtotime($currDay)) . "</th></tr>";
        if (!isset($prevDay))
            $out .= "<tr><th>" . date('D',  strtotime($currDay)) . "</th></tr>";

        $prevDay = $currDay;

        $extraBeg[$it] = DateTime::createFromFormat('H:i:s', $extraBeg[$it]);
        $extraBeg[$it] = date_format($extraBeg[$it], 'H:i');
        $extraEnd[$it] = DateTime::createFromFormat('H:i:s', $extraEnd[$it]);
        $extraEnd[$it] = date_format($extraEnd[$it], 'H:i');
        $out .= "<tr><td>" . $extraBeg[$it] . "</td><td>" . $extraEnd[$it] . "</td><td>" . $extraDesc[$it] . "</td></tr>";
        $it++;
    }

    $conn->close();
    $out .= "</table>";
    return $out;
}
