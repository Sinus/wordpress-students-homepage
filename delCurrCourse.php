<?php
/*
 *  *Plugin Name: Delete Current Course
 *  Author: Marcin Dominiak
 */

add_shortcode('DelCurrCourse', 'delCurrCourse');

function delCurrCourse()
{
    $myId = $_COOKIE['currID'];
    $url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

    $conn = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
    if ($conn->connect_error)
    {
        die("Connection failed: ". $conn->connect_error);
    }


    $out = "";

    if (!empty($_POST['checkList']))
    {

        foreach($_POST['checkList'] as $check)
        {
            $check = intval($check);
            $res2 = $conn->query("UPDATE User_Courses SET active = 0 
                WHERE courseID = '$check'");
        }
        return $out;
    }

    $res = $conn->query("SELECT courseID FROM User_Courses WHERE
        userID = '$myId' AND active = 1");

    $out = "<form action = " . $url . " method = \"POST\">";
    while ($row = $res->fetch_array())
    {
        $cID = $row['courseID'];
        $res2 = $conn->query("SELECT name FROM Courses WHERE ID = '$cID'");
        $row2 = $res2->fetch_array();
        $nam = $row2['name'];

        $out .= "<input type = \"checkbox\" name = \"checkList[]\"
            value = \"" . $row['courseID'] . "\">" . $nam . "<br />";
    }
    $out .= "<input type = \"submit\" />";
    $out .= "</form>";
    $conn->close();
    return $out;
}
