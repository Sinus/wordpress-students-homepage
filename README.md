Zaimplementowane shortcody wraz z opisem:
[WandI]
Wyswietla historie zatrudnienia

[AddWork]
Formularz pozwalajacy na dodanie historii zatrudnienia

[Projects]
Wyswietla historie projektow w ktych dany czlowiek bral udzial

[AddProjects]
Formularz do dodawania projektow

[Technologies]
Wyswietla znane technologie

[AddTechnologies]
Formularz do dodawania technologii

[Prizes]
Wyswietla otrzymane nagrody

[AddPrizes]
Formularz do dodawania nagrod

[AddCurrCourse]
Zmienia stan przedmiotu na aktywny

[DelCurrCourse]
Zmienia stan przedmiotu na nieaktywny

[AddCourse]
Dodaje przedmiot do listy realizowanych przedmiotow.

[AddExtraPlan]
Formularz do dodawania aktywnosci do planu zajec

[ActiveCourses]
Wyswietla aktywne przedmioty

[InactiveCourses]
Wyswietla niaktywne przedmioty (ale ktore kiedys byly aktywne)

[Plan]
Wsywietla plan - przedmioty + zdarzenia zdefiniowane przez uzytkownika

[PrintCV]
Wyswietla CV - do ktore elementy sa wyswietlane zalezy od tego, uzytkownik
ustawi w formularzu udostepnionym przez [SetCV].

[SetCV]
Opisane w [PrintCV]

Uruchomienie pluginow: wystarczy zainstalowac wszystkie pluginy w wordpressie
i shortcody beda dzialac
Dodatkowe zaleznosci: brak
Cudz kod w projekcie - Wordpress Social Login zmodyfikowany przez Henryka
Michalewskiego (dodana funkcjonalnosc z USOSem)
