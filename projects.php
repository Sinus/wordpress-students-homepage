<?php
/*
 *  *Plugin Name: Projects
 *  Author: Marcin Dominiak
 */

add_shortcode('Projects', 'projects');

function projects()
{
    $myId = $_COOKIE['currID'];
    $conn = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

    if ($conn->connect_error)
    {
        die("Connection failed: ". $conn->connect_error);
    }

    $out = "<table>";
    $out .= "<tr><td>Opis</td><td>Link</td></tr>";-
    $res = $conn->query("SELECT * FROM Projects WHERE ID = '$myId'");
    while($row = $res->fetch_array())
    {
        $out .= "<tr><td>" . $row['description'] . "</td><td>";
        $out .= "<a href = \"" . $row['link'] ."\">" . "link" .
            "</a></td></tr>";
    }
    $conn->close();
    $out .= "</table>";
    return $out;
}

