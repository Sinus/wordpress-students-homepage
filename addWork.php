<?php
/*
 *  Plugin Name: Add work
 *  Author: Marcin Dominiak
 */

add_shortcode('AddWork', 'addWork');

function addWork()
{
    $myId = $_COOKIE['currID'];
    $conn = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

    $url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

    if ($conn->connect_error)
    {
        die("Connection failed: ". $conn->connect_error);
    }

    $out = "";
    if ((isset($_POST['usr_name'])) && (isset($_POST['usr_begin'])) && 
        (isset($_POST['usr_end'])) && (isset($_POST['usr_link'])) &&
        (isset($_POST['usr_desc'])))
    {
        $sanitizedName = filter_var($_POST['usr_name'], FILTER_SANITIZE_STRING);
        $sanitizedBeg = filter_var($_POST['usr_begin'], FILTER_SANITIZE_STRING);
        $sanitizedEnd = filter_var($_POST['usr_end'], FILTER_SANITIZE_STRING);
        $sanitizedLink = filter_var($_POST['usr_link'], FILTER_SANITIZE_URL);
        $sanitizedDesc = filter_var($_POST['usr_desc'], FILTER_SANITIZE_STRING);

        $res = $conn->query("INSERT INTO Work VALUES (
            '$myId', '$sanitizedName', '$sanitizedBeg', '$sanitizedEnd',
            '$sanitizedLink', '$sanitizedDesc')");

        if ($res)
            $out .= "Pomyslnie dodano wartosci!<br />";
        else
            $out .= "Wystapil blad.<br />";
    }

    $out .= "<form action=" . $url . " method=\"POST\">";
    $out .= "Nazwa firmy:<br />";
    $out .= "<input type = \"text\" name = \"usr_name\">";
    $out .= "<br />";
    $out .= "Paczatek:<br />";
    $out .= "<input type = \"date\" name = \"usr_begin\">";
    $out .= "<br />";
    $out .= "Koniec:<br />";
    $out .= "<input type = \"date\" name = \"usr_end\">";
    $out .= "<br />";
    $out .= "Link:<br />";
    $out .= "<input type = \"text\" name = \"usr_link\">";
    $out .= "<br />";
    $out .= "Opis:<br />";
    $out .= "<input type = \"text\" name = \"usr_desc\">";
    $out .= "<br /><br />";
    $out .= "<input type = \"submit\" value = \"Submit\">";
    $out .= "</form>";

    $conn->close();
    return $out;
}
