<?php
/*
 *  Plugin Name: Courses
 *  Author: Marcin Dominiak
 */

add_shortcode('ActiveCourses', 'activeCourses');
add_shortcode('InactiveCourses', 'invactiveCourses');

function getCourses($act)
{
    $myId = $_COOKIE['currID'];
    $conn = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

    if ($conn->connect_error)
    {
        die("Connection failed: ". $conn->connect_error);
    }


    $out = "<table>";
    $out .= "<tr><td>Nazwa</td><td>Opis</td></tr>";
    $res = $conn->query("SELECT name, ID, description FROM User_Courses 
        JOIN Courses ON courseID = ID WHERE userID ='$myId' AND active = '$act'");
    while($row = $res->fetch_array())
    {
        $out .= "<tr><td>" . $row['name'] . '</td><td>' . $row['description'] . '</td></tr>';
    }
    $conn->close();
    $out .= "</table>";
    return $out;
}

function activeCourses()
{
    return getCourses(1);
}

function invactiveCourses()
{
    return getCourses(0);
}