<?php
/*
 *  Plugin Name: Add course
 *  Author: Marcin Dominiak
 */

add_shortcode('AddCourse', 'addCourse');

function addCourse()
{
    $conn = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
    if ($conn->connect_error)
    {
        die("Connection failed: ". $conn->connect_error);
    }

    $date = new DateTime();
    $newID = $date->getTimestamp();
    $newID = $newID->format('ymdhis');

    $out = "";
    if ((isset($_POST['usr_name'])))
    {
        $sanitizedName = filter_var($_POST['usr_name'], FILTER_SANITIZE_STRING);
        $sanitizedDesc = "";
        if (isset($_POST['usr_desc']))
            $sanitizedDesc = filter_var($_POST['usr_desc'], FILTER_SANITIZE_STRING);

        $res = $conn->query("INSERT INTO Courses VALUES (
            '$newID', '$sanitizedName', '$sanitizedDesc')");

        if ($res)
            $out .= "Pomyslnie dodano wartosci!<br />";
        else
            $out .= "Wystapil blad.<br />";
    }

    $out .= "<form action=" . $url . " method=\"POST\">";
    $out .= "Nazwa uczonej sie rzeczy:<br />";
    $out .= "<input type = \"text\" name = \"usr_name\">";
    $out .= "<br />";
    $out .= "Opis:<br />";
    $out .= "<input type = \"text\" name = \"usr_desc\">";
    $out .= "<br /><br />";
    $out .= "<input type = \"submit\" value = \"Submit\">";
    $out .= "</form>";

    $conn->close();
    return $out;
}
