<?php
/*
 *  *Plugin Name: Prizes
 *  Author: Marcin Dominiak
 */

add_shortcode('Prizes', 'prizes');

function prizes()
{
    $myId = $_COOKIE['currID'];
    $conn = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

    if ($conn->connect_error)
    {
        die("Connection failed: ". $conn->connect_error);
    }


    $out = "<table>";
    $out .= "<tr><td>Opis</td><td>Data</td><td>Link</td></tr>";
    $res = $conn->query("SELECT * FROM Prizes WHERE ID = '$myId'");
    while($row = $res->fetch_array())
    {
        $out .= "<tr><td>" . $row['description'] . "</td><td>";
        $out .= $row['when_received'] . "</td><td>";
        $out .= "<a href = \"" . $row['link'] . "\">" . "Link" . "</a></td>";
        $out .= "</tr>";
    }
    $conn->close();
    $out .= "</table>";
    return $out;
}

