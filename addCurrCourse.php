<?php
/*
 *  Plugin Name: Add Current Course
 *  Author: Marcin Dominiak
 */

add_shortcode('AddCurrCourse', 'addCurrCourse');

function addCurrCourse()
{

    $conn = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

    $url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    if ($conn->connect_error)
    {
        die("Connection failed: ". $conn->connect_error);
    }


    $out = "";

    if (!empty($_POST['checkList']))
    {
        foreach($_POST['checkList'] as $check)
        {
            $res2 = $conn->query("SELECT * FROM User_Courses
                WHERE userID = '$myId' AND courseID = '$check'");
            if ($res2->num_rows == 0) //this course was not active before
                $q = "INSERT INTO User_Courses VALUES (
                    '$myId', '$check', 1)";
            else
                $q = "UPDATE User_Courses SET active = 1
                WHERE courseID = '$check' AND userID = '$myId'";
            $res3 = $conn->query($q);
        }
        return $out;
    }

    $res = $conn->query("SELECT DISTINCT ID, name FROM Courses WHERE ID 
        NOT IN (SELECT courseID FROM User_Courses WHERE active = 1)");

    $out = "<form action = " . $url . " method = \"POST\">";
    while ($row = $res->fetch_array())
    {
        $out .= "<input type = \"checkbox\" name = \"checkList[]\"
            value = \"" . $row['ID'] . "\">" . $row['name'] . "<br />";
    }
    $out .= "<input type = \"submit\" />";
    $out .= "</form>";
    $conn->close();
    return $out;
}
