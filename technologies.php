<?php
/*
 *  Plugin Name: Technologies
 *  Author: Marcin Dominiak
 */

add_shortcode('Technologies', 'technologies');

function technologies()
{
    $myId = $_COOKIE['currID'];
    $conn = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

    if ($conn->connect_error)
    {
        die("Connection failed: ". $conn->connect_error);
    }


    $out = "<table>";
    $out .= "<tr><td>Technologia</td><td>Doświadczenie</td><td>Link</td></tr>";
    $res = $conn->query("SELECT * FROM Technologies WHERE ID = '$myId'");
    while($row = $res->fetch_array())
    {
        $out .= "<tr><td>" . $row['name'] . "</td><td>" . 
            $row['expirience'] . "</td><td><a href = \"" . $row['link'] . "\">" . "link" .
            "</a></td></tr>";
    }
    $conn->close();
    $out .= "</table>";
    return $out;
}

