<?php
/*
 *  Plugin Name: Add Extra Activities
 *  Author: Marcin Dominiak
 */

add_shortcode('AddExtraPlan', 'addExtraPlan');

function addExtraPlan()
{
    $myId = $_COOKIE['currID'];
    $conn = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

    $url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

    if ($conn->connect_error)
    {
        die("Connection failed: ". $conn->connect_error);
    }


    $out = "";
    if ((isset($_POST['usr_date'])) && (isset($_POST['usr_beg'])) &&
        (isset($_POST['usr_end'])) && (isset($_POST['usr_desc'])))
    {
        $sanitizedDate = filter_var($_POST['usr_date'], FILTER_SANITIZE_STRING);
        $sanitizedBeg = filter_var($_POST['usr_beg'], FILTER_SANITIZE_STRING);
        $sanitizedEnd = filter_var($_POST['usr_end'], FILTER_SANITIZE_STRING);
        $sanitizedDesc = filter_var($_POST['usr_desc'], FILTER_SANITIZE_STRING);

        $res = $conn->query("INSERT INTO Extra_Plan VALUES (
            '$myId', '$sanitizedBeg', '$sanitizedEnd', '$sanitizedDate',
            '$sanitizedDesc')");

        if ($res)
            $out .= "Pomyslnie dodano wartosci!<br />";
        else
            $out .= "Wystapil blad.<br />";
    }

    $out .= "<form action=" . $url . " method=\"POST\">";
    $out .= "Dzien:<br />";
    $out .= "<input type = \"date\" name = \"usr_date\">";
    $out .= "<br />";
    $out .= "Godzina rozpoczecia:<br />";
    $out .= "<input type = \"time\" name = \"usr_beg\">";
    $out .= "<br />";
    $out .= "Godzina zanczenia:<br />";
    $out .= "<input type = \"time\" name = \"usr_end\">";
    $out .= "<br />";
    $out .= "Czynnosc:<br />";
    $out .= "<input tupe = \"text\" name = \"usr_desc\">";
    $out .= "<br /><br />";
    $out .= "<input type = \"submit\" value = \"Submit\">";
    $out .= "</form>";

    $conn->close();
    return $out;
}
