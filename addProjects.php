<?php
/*
 *  Plugin Name: Add projects
 *  Author: Marcin Dominiak
 */

add_shortcode('AddProjects', 'addProjects');

function addProjects()
{
    $myId = $_COOKIE['currID'];
    $conn = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

    $url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

    if ($conn->connect_error)
    {
        die("Connection failed: ". $conn->connect_error);
    }


    $out = "";
    if ((isset($_POST['usr_name'])) && (isset($_POST['usr_link'])))
    {
        $sanitizedName = filter_var($_POST['usr_name'], FILTER_SANITIZE_STRING);
        $sanitizedLink = filter_var($_POST['usr_link'], FILTER_SANITIZE_URL);

        $res = $conn->query("INSERT INTO Projects VALUES (
            '$myId', '$sanitizedLink', '$sanitizedName')");

        if ($res)
            $out .= "Pomyslnie dodano wartosci!<br />";
        else
            $out .= "Wystapil blad.<br />";
    }

    $out .= "<form action=" . $url . " method=\"POST\">";
    $out .= "Opis projektu:<br />";
    $out .= "<input type = \"text\" name = \"usr_name\">";
    $out .= "<br />";
    $out .= "Link do projektu:<br />";
    $out .= "<input type = \"text\" name = \"usr_link\">";
    $out .= "<br /><br />";
    $out .= "<input type = \"submit\" value = \"Submit\">";
    $out .= "</form>";

    $conn->close();
    return $out;
}
