<?php
/*
 *  Plugin Name: Work and Internship
 *  Author: Marcin Dominiak
 */

add_shortcode('WandI', 'workInt');

function workInt()
{
    $myId = $_COOKIE['currID'];
    $conn = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

    if ($conn->connect_error)
    {
        die("Connection failed: ". $conn->connect_error);
    }


    $out = "<table>";
    $out .= "<tr><td>Opis</td><td>Firma</td><td>Od kiedy</td><td>Do kiedy</td><td>Link</td></tr>";
    $res = $conn->query("SELECT * FROM Work WHERE ID = '$myId'");
    while($row = $res->fetch_array())
    {
        $out .= "<tr><td>" . $row['description'] . "</td><td>" . $row['company'] . "</td><td>" . 
            $row['beggining'] . "</td><td>" . $row['finish'] . 
            "</td><td><a href = \"" . $row['link'] . "\">" . "link" . "</a></td></tr>";
    }
    $conn->close();
    $out .= "</table>";
    return $out;
}

