<?php
/*
 *  Plugin Name: PrintCV
 *  Author: Marcin Dominiak
 */

add_shortcode('PrintCV', 'printCV');
add_shortcode('SetCV', 'setCV');

function printCV()
{
    $myId = $_COOKIE['currID'];
    $conn = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

    $url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    if ($conn->connect_error)
    {
        die("Connection failed: ". $conn->connect_error);
    }


    $out = "";

    $res = $conn->query("SELECT * FROM CV WHERE ID = '$myId'");

    while ($row = $res->fetch_array())
    {
        $job = $row['work'];
        $tech = $row['technologies'];
        $proj = $row['projects'];
        $priz = $row['prizes'];

        if ($job)
            $out .= do_shortcode('[WandI]');
        if ($tech)
            $out .= do_shortcode('[Technologies]');
        if ($proj)
            $out .= do_shortcode('[Projects]');
        if ($priz)
            $out .= do_shortcode('[Prizes]');
    }

    return $out;

}

function setCV()
{
    $myId = $_COOKIE['currID'];
    $servername = "labdb.mimuw.edu.pl";
    $username = "md346906";
    $password = "hierBavdieneegs2";
    $url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

    $conn = new mysqli($servername, $username, $password, "md346906");
    if ($conn->connect_error)
    {
        die("Connection failed: ". $conn->connect_error);
    }


    $out = "";
    if (!empty($_POST['checkList']))
    {
        $conn->query("UPDATE CV SET work = 0, technologies = 0, projects = 0, prizes = 0 WHERE ID = '$myId'");
        foreach($_POST['checkList'] as $check)
        {
            if ($check == "Zatrudnienie")
            {
                $conn->query("UPDATE CV SET work = 1 WHERE ID = '$myId'");
            }
            else if ($check == "Technologie")
            {
                $conn->query("UPDATE CV SET technologies = 1 WHERE ID = '$myId'");
            }
            else if ($check == "Projekty")
            {
                $conn->query("UPDATE CV SET projects = 1 WHERE ID = '$myId'");
            }
            else if ($check == "Nagrody")
            {
                $conn->query("UPDATE CV SET prizes = 1 WHERE ID = '$myId'");
            }
        }
        return $out;
    }


    $out .= "<form action = " . $url  . " method = \"POST\">";  
    $out .= "<input type = \"checkbox\" name = \"checkList[]\" 
        value = \"Zatrudnienie\"> Zatrudnienie/Staz <br />";
    $out .= "<input type = \"checkbox\" name = \"checkList[]\"
        value = \"Technologie\"> Znane technologie <br />";
    $out .= "<input type = \"checkbox\" name = \"checkList[]\"
        value = \"Projekty\"> Projekty <br />";
    $out .= "<input type = \"checkbox\" name = \"checkList[]\"
        value = \"Nagrody\"> Nagrody <br />";
    $out .= "<input type = \"submit\" />";
    $out .= "</form>";


    $conn->close();
    return $out;
}

